#!/usr/bin/perl

use strict;
use warnings;

my @filelist=split('\n',`find /home/Share/automatic_upload_samplebased/logs/prod_*_Panel_74_SGE_new_files_list.log -mtime -2`);
for(my $i=0;$i<=$#filelist;$i++){
	my $file=(split /\//, $filelist[$i])[-1];
	my $runid=substr($file,index($file,"Nextseq_")+8);
	$runid=substr($runid,0,index($runid,"_Aligned_"));
	next if(!-s "/NGSData/GRSDeposit/74_CancerV4/$runid");#not 4.5 run
#	printf "$i $runid\n";
	next if(-e "/NGSData/GRSDeposit/74_CancerV4/PRS_bycronjob/$runid/.rundone");#already done
	`/mnt/dxu/clinbot_dev/calcprs_onerun.pl $runid`;
}

