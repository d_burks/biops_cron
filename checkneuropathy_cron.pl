#!/usr/bin/perl

use strict;
use warnings;
use DBI;
my @filelist=split('\n',`find /home/Share/automatic_upload_samplebased/logs/prod_NGS_*_Aligned_Panel_9801_SGE_db_log.log -mtime -3`);#younger than 1 day
for (my $i=0;$i<=$#filelist;$i++){
	my @cont=split(/\n/,`cat $filelist[$i]`);
	for(my $j=0;$j<=$#cont;$j++){
		my @cont2=split(/\t/,$cont[$j]);
		my $runid=(split / /,$cont2[0])[0];
		my $samplefolder=$cont2[1];
		my $accid=(split /\//,$samplefolder)[-1];
		$accid=~ s/Sample_//;
		my $desfolder="/NGSData/BamDeposit/9801_Neuropathy/$runid";
		if(!-e $desfolder){`mkdir -p $desfolder`;}
		if(!-e "$desfolder/$accid.bam"){
		#	printf "$j $runid $samplefolder $accid\n";
			`cp $samplefolder/$accid.bam $desfolder/`;
			`cp $samplefolder/$accid.bam.bai $desfolder/`;
		}
	}
}



