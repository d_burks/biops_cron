#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime);
#my $date = strftime "%Y%m%d", localtime;
my @filelist=split('\n',`find /NGSData/TumorNext/Results/*/*reviewed.xlsx /NGSData/TumorNext/Results/*/*Reviewed.xlsx -mtime -1`);#younger than 1 day
for(my $i=0;$i<=$#filelist;$i++){
	my @content=split(/\//,$filelist[$i]);
	my $runid=$content[4];
	my $first=substr($content[$#content],0,1);
	next if($first eq "~");
	my $path="/NGSData/TumorNext/Results/$runid/N-of-one-files";
#$path="/home/dxu/mito/nof1/N-of-one-files";
	my $error="";
	my $processed="";
	if(-e $path){goto posoutput;}#do nothing if N-of-one exists
	mkdir "$path";
	if(!-e $path) {$error.="Can't create folder $path.\n";goto posoutput;}
	chdir "$path";
	`/mnt/swu/bin/anaconda2/bin/python /mnt/clinical/bin/SomaticPipeline/V101/convert.py --file \"$filelist[$i]\" > stat.txt`;
	$processed.="$path\n";
	my @stats=split(/\n/,`cat stat.txt`);
	my $nxml=$#stats;
	if($nxml<=0){$error.="No xml files were generated.\nPlease check the xlsx file \\\\usav1isip2\\data\\TumorNext\\Results\\$runid\\$content[5].\n";}
	####
    posoutput:;
    if($processed ne "" && $nxml>0){
#	my $to = 'dxu@ambrygen.com';
	my $to = 'dchen@ambrygen.com, hvuong@ambrygen.com, swu@ambrygen.com, dxu@ambrygen.com';
	my $from = 'dxu@ambrygen.com';
	my $subject = "N-of-One xml files are ready for run $runid";
	open(MAIL, "|/usr/sbin/sendmail -t");
	print MAIL "To: $to\n";
	print MAIL "From: $from\n";
	print MAIL "Content-Type: text/html\n";
	print MAIL "Subject: $subject\n\n";
	my $message="
<html><head><style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 2px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
</style></head><body>
Dear All:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N-of-One xml files are ready at \\\\usav1isip2\\data\\TumorNext\\Results\\$runid\\N-of-one-files\\.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please upload those xml files to N-of-One website. Basic statistics of the samples in this run are given below:<br><br>";
	$message.="<table><tr><th>Sample_Name</th><th>Somatic</th><th>Fusion</th><th>CNV</th><th>Germline</th></tr>";
	for(my $j=1;$j<=$#stats;$j++){
		my @values=split(/\t/,$stats[$j]);
		$message.="<tr><td>$values[0]</td><td>$values[1]</td><td>$values[2]</td><td>$values[3]</td><td>$values[4]</td></tr>";
	}
	$message.="</table></body></html><br>Thanks<br>";
	print MAIL $message;
	close(MAIL);
    }
    elsif($error ne ""){
#	my $to = 'dxu@ambrygen.com';
	my $to = 'swu@ambrygen.com, hvuong@ambrygen.com, dxu@ambrygen.com';
	my $from = 'dxu@ambrygen.com';
	my $subject = "No N-of-One xml files were generated for run $runid";
	open(MAIL, "|/usr/sbin/sendmail -t");
	print MAIL "To: $to\n";
	print MAIL "From: $from\n";
	print MAIL "Subject: $subject\n\n";
	print MAIL $error;
	close(MAIL);
    }
}


exit;


