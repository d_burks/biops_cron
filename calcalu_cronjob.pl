#!/usr/bin/perl

use strict;
use warnings;

my @filelist=split('\n',`find /home/Share/automatic_upload_samplebased/logs/prod_*_Panel_77_SGE_new_files_list.log -mtime -2`);
for(my $i=0;$i<=$#filelist;$i++){
	my $file=(split /\//, $filelist[$i])[-1];
	my $runid=substr($file,index($file,"Nextseq_")+8);
	$runid=substr($runid,0,index($runid,"_Aligned_"));
#	printf "$i $runid\n";
	next if(-e "/NGSData/GRSDeposit/77_CancerV5/ALU_bycronjob/$runid/.rundone");#already done
	next if(-e "/NGSData/GRSDeposit/77_CancerV5/ALU_bycronjob/$runid/.runstart");#already started
	`/mnt/dxu/clinbot_dev/calcalu_onerun.pl $runid`;
	#printf "/mnt/dxu/clinbot_dev/calcalu_onerun.pl $runid\n";
}

