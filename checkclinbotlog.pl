#!/usr/bin/perl

use strict;
use warnings;

my $folder="/mnt/clinical/bin/Scripts/Clinbot210";
my @date = split(" ", localtime(time));
my $day = $date[0]; 
#printf "$day\n";
my $logfile="$folder/$day\.log";
if(!-s $logfile){exit;}
my @recordtmp=("na1","na2","na3","na4");
my @record=();
open(my $file, "$logfile");
while(my $line=<$file>){
	for(my $i=0;$i<3;$i++){$recordtmp[$i]=$recordtmp[$i+1];}
	$recordtmp[3]=$line;
	if($recordtmp[3]=~ /running cycle:/){
		for(my $i=0;$i<4;$i++){$record[$i]=$recordtmp[$i];}
	}
}
close($file);
if(!defined $record[3]){exit;}
if(!($record[3]=~ /running cycle:/)){exit;}
if($record[0]=~ /na/ && $record[1]=~ /na/ && $record[2]=~ /na/) {exit;}
if($record[0]=~ /GO FOR PIPELINE/ || $record[1]=~ /GO FOR PIPELINE/ || $record[2]=~ /GO FOR PIPELINE/) {exit;}
if($record[0]=~ /pipeline job written/ || $record[1]=~ /pipeline job written/ || $record[2]=~ /pipeline job written/) {exit;}
if(!($record[1]=~/-------------------------------------/)){
	#error 
	$record[0] =~ s/\s|\||\<|\>|\"|\'|\;|\&|\`|\!|\$|\(|\)/_/g;
	$record[1] =~ s/\s|\||\<|\>|\"|\'|\;|\&|\`|\!|\$|\(|\)/_/g;
	$record[2] =~ s/\s|\||\<|\>|\"|\'|\;|\&|\`|\!|\$|\(|\)/_/g;
#	printf "last 3 lines:\n$record[0] $record[1] $record[2] URGENT Clinbot $record[3]\n";
	my $email_command = "echo 'last 3 lines:\n$record[0] $record[1] $record[2]' | mailx -s 'URGENT Clinbot $record[3]' tkitapci\@ambrygen.com";
	my $send_email = `$email_command`;	

}


