#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime);
my $date = strftime "%Y%m%d", localtime;
#$date="20160904";
my $failed=`/mnt/swu/avaautoupload_deletion/checkundeleted_cron.pl $date`;
my @content=split(/\n/,$failed);
my @pending1=open_dir("/mnt/swu/avaautoupload_deletion/Input");
my @content2;
my $pending="";
for(my $i=0;$i<=$#pending1;$i++){
	next if($pending1[$i]=~ /lock/);
	push @content2, $pending1[$i];
	$pending.="/mnt/swu/avaautoupload_deletion/Input/$pending1[$i]\n";
}
#printf "$#content $#content2\n";
#exit();
if($#content==-1 && $#content2<10) {exit();}
    my $to = 'tkitapci@ambrygen.com';
    my $from = 'tkitapci@ambrygen.com';
    my $subject = "failed_deletion $#content+1 pending_deletion $#content2+1 on $date";
    my $message;
    if($#content>=0){
	$message .="failed_deletion $#content+1:\n$failed";
    }
    if($#content2>=10){
	$message .="pending_deletion $#content2+1:\n$pending";
    }
    open(MAIL, "|/usr/sbin/sendmail -t");
  # Email Header
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "Subject: $subject\n\n";
  # Email Body
    print MAIL $message;
    close(MAIL);

exit;

sub open_dir {

    my($dirname) = @_;
    my $fh;
    my @files;

    unless(opendir($fh, $dirname)) {
        print "Cannot open directory $dirname\n";
        exit;
    }

    @files = grep ( !/^\.\.?$/, readdir $fh);

    closedir $fh;

    return @files;
}




