#!/mnt/clinical/bin/miniconda3/bin/python
'''
python3 pipeline_file_scan.py

Pipeline file monitoring script.  Initial list of pipeline-related files scraped
from bitbucket repo and pipeline log files in June 2021. 

If file modifications are detected (size,date modified) in any pipeline file,
a warning is generated and written to the warnings.txt file.  If the modified 
file is below fs_thresh_bytes, an alert is also generated and written to the
alerts.txt file.

Output directories and pipeline file lists are loaded through a .env file.

'''

from pathlib import Path
from dotenv import load_dotenv
import os
import time
import datetime

load_dotenv()

timestamp = time.strftime('%l:%M%p %Z - %b %d, %Y')

#Set size threshold for modified files to go into alert log 
fs_thresh_bytes = 300

dir = Path(os.getenv("PIPELINE_FILE_SCAN_OUTPUT_DIR"))
pipeline_files_input_file = Path(os.getenv("PIPELINE_FILE_SCAN_LIST"))
full_log_file = dir.joinpath("full_log.txt")
alert_log_file = dir.joinpath("alerts.txt")
warn_log_file = dir.joinpath("warnings.txt")
missing_log_file = dir.joinpath("missing.txt")


dir.mkdir(exist_ok=True)

class PipelineFile:

    def __init__(self, filename):
        self.filename = Path(filename)

    def add_size(self, size):
        self.src_file_size = int(size)

    def add_mtime(self, mtime):
        self.src_file_mtime = float(mtime)

    def get_cur_file_stats(self):
        f_stats = self.filename.stat()
        self.cur_file_size = f_stats.st_size
        self.cur_file_mtime = f_stats.st_mtime

    def is_size_diff(self):
        return self.src_file_size != self.cur_file_size

    def is_mtime_diff(self):
        return self.src_file_mtime != self.cur_file_mtime

def get_timestamp_as_str(ts):

    # Convert to datetime object
    dt = datetime.datetime.fromtimestamp(ts)

    # Return as '2021-07-19 10:01:51'
    return dt.strftime("%Y-%m-%d %H:%M:%S")

def get_filesize_as_str(fs):

    for unit in ["", "KB", "MB"]:
        if fs < 1024.0:
            return f"{fs:.3f} {unit}"
        fs /= 1024.0
    return f"{fs:.3f} GB"
    
def write_altered_file_logs(filelist,logfile,timestamp):
    with open(logfile,'a') as outlogfile:
        outlogfile.write(f'Files altered as of {timestamp}\n')
        for f in filelist:
            outlogfile.write(
                f'{f.filename} has been altered!\n'
                f'    source:    {get_timestamp_as_str(f.src_file_mtime)}    {get_filesize_as_str(f.src_file_size)}\n'
                f'    current:   {get_timestamp_as_str(f.cur_file_mtime)}    {get_filesize_as_str(f.cur_file_size)}\n'
            )
        outlogfile.write("\n")


ct_tot_files = 0
pipefiles = []
missfiles = []
alertfiles = []
warnfiles = []

with open(pipeline_files_input_file) as infile:
    infile.readline()
    for lines in infile:
        ct_tot_files += 1
        lines = lines.rstrip().split("\t")

        # Create the PipelineFile Object
        pipefile = PipelineFile(lines[0])
        pipefile.add_size(lines[1]) 
        pipefile.add_mtime(lines[2]) 

        if pipefile.filename.is_file():
            pipefile.get_cur_file_stats() 
            pipefiles.append(pipefile)
            if pipefile.is_size_diff() or pipefile.is_mtime_diff():
                warnfiles.append(pipefile)
                if pipefile.cur_file_size < fs_thresh_bytes:
                    alertfiles.append(pipefile)
        else:
            missfiles.append(pipefile)

# Write the missingfiles
if missfiles:
    with open(missing_log_file, 'a') as misslogfile:
        misslogfile.write(f'Files missing as of {timestamp}\n')
        for m in missfiles: 
            misslogfile.write(f'{timestamp} : {m.filename} no longer exists!\n')
        misslogfile.write("\n")

# Write the altered files that are also less than fs_thresh_bytes in size.
if alertfiles:
    write_altered_file_logs(alertfiles,alert_log_file)
        
# Write any of the altered files regardless of current size.
if warnfiles:
    write_altered_file_logs(warnfiles,warn_log_file,timestamp)

# Write the full log
with open(full_log_file, 'a') as logfile:

    logfile.write(f'File status as of {timestamp}\n')
    logfile.write(f'Pipeline File List: {pipeline_files_input_file}\n')
    
    # Write out the summary
    logfile.write(
        f"{ct_tot_files - len(missfiles) - len(alertfiles):>4} Files OK\n"
        f"{len(missfiles):>4} Missing Files\n"
        f"{len(alertfiles):>4} Altered Files\n"
        f"-----------------\n"
        f"{ct_tot_files:>4} Total Files Checked\n\n"
    )

    # Write out the full log
    for p in pipefiles:

        # Get the file output
        lout = (os.popen(f'ls -lhat {p.filename}').read()).rstrip()
        logfile.write(lout + '\n')
    
    logfile.write('\n')

