#!/usr/bin/perl

use strict;
use warnings;

my $path = "/mnt/swu/avaautoupload_deletion";
#my $date = `date --date="today" +"%Y%m%d"`;
#chomp $date;
#my $log = 'log' . $date;

# Dev
my $dir = "$path/logs_dev";
my @files = open_dir($dir);
foreach my $file (@files){
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };

    my $to = 'testpipelinedashboard@ambrygen.com';
    my $from = 'tkitapci@ambrygen.com';
    my $subject = 'These accessions(panel_id) have been deleted upon request';
    my $message = "This is an email from ava3-dev for deletion request of autoupload\n";
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	$message .= "$l[0](panel_id: $l[1])\t$l[2]\n";	
    }
    close $fh or warn "$0: close $df: $!";

    open(MAIL, "|/usr/sbin/sendmail -t");
  # Email Header
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "cc: sli\@ambrygen.com\n";
    print MAIL "Subject: $subject\n\n";
  # Email Body
    print MAIL $message;
    close(MAIL);

    system("mv $df $path/Processed_dev");
}



# Test
$dir = "$path/logs_test";
@files = open_dir($dir);
foreach my $file (@files){
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };

    my $to = 'testpipelinedashboard@ambrygen.com';
    my $from = 'tkitapci@ambrygen.com';
    my $subject = 'These accessions(panel_id) have been deleted upon request';
    my $message = "This is an email from ava3-test for deletion request of autoupload\n";
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);


	$message .= "$l[0](panel_id: $l[1])\t$l[2]\n";
    }
    close $fh or warn "$0: close $df: $!";

    open(MAIL, "|/usr/sbin/sendmail -t");
  # Email Header
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "cc: sli\@ambrygen.com\n";
    print MAIL "Subject: $subject\n\n";
  # Email Body
    print MAIL $message;
    close(MAIL);

    system("mv $df $path/Processed_test");
}


# Stage
$dir = "$path/logs_stage";
@files = open_dir($dir);
foreach my $file (@files){
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };

    my $to = 'testpipelinedashboard@ambrygen.com';
    my $from = 'tkitapci@ambrygen.com';
    my $subject = 'These accessions(panel_id) have been deleted upon request';
    my $message = "This is an email from ava3-staging for deletion request of autoupload\n";
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	$message .= "$l[0](panel_id: $l[1])\t$l[2]\n";
    }
    close $fh or warn "$0: close $df: $!";

    open(MAIL, "|/usr/sbin/sendmail -t");
  # Email Header
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "cc: sli\@ambrygen.com\n";
    print MAIL "Subject: $subject\n\n";
  # Email Body
    print MAIL $message;
    close(MAIL);

    system("mv $df $path/Processed_stage");
}





# Production
$dir = "$path/logs";
@files = open_dir($dir);
foreach my $file (@files){
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };

    my $to = 'pipelinedashboard@ambrygen.com';
    my $from = 'tkitapci@ambrygen.com';
    my $subject = 'These accessions(panel_id) have been deleted upon request';
    my $message = "This is a summary email for deletion request of autoupload\n";
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	$message .= "$l[0](panel_id: $l[1])\t$l[2]\n";
    }
    close $fh or warn "$0: close $df: $!";

    open(MAIL, "|/usr/sbin/sendmail -t");
  # Email Header
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "cc: sli\@ambrygen.com\n";
    print MAIL "Subject: $subject\n\n";
  # Email Body
    print MAIL $message;
    close(MAIL);

    system("mv $df $path/Processed");
}




exit;



###########################################################################
# Subroutines
###########################################################################

# open_dir
#
#   - given dirname, set dirhandle

sub open_dir {

    my($dirname) = @_;
    my $fh;
    my @files;

    unless(opendir($fh, $dirname)) {
        print "Cannot open directory $dirname\n";
        exit;
    }

    @files = grep ( !/^\.\.?$/, readdir $fh);

    closedir $fh;

    return @files;
}
