#!/usr/bin/perl
use strict;
use warnings;

my $path = "/mnt/swu/avaautoupload_deletion";
my $date = `date --date="today" +"%Y%m%d"`;
chomp $date;
my $log = 'log' . $date;


# Dev
my $dir = "$path/Input_dev";
my $server = 'usav1svsqld70';
my @files = open_dir($dir);
unless(-e "$dir/lock"){
    system("echo '' > $dir/lock"); 
    foreach my $file (@files){
	my $df = "$dir/$file";
	open my $fh, "<", $df or do {
	    warn "$0: open $df: $!";
	    next;
	};
	while(<$fh>){
	    chomp;
	    my @l = split(/\t/, $_);
	    $l[1] =~ s/\s/_/g;	    	    
	    my $cmd = "perl $path/remove_accession.pl $l[0] $l[1] $l[2] $server >> $path/logs_dev/$log";
#	    print "$cmd\n";
	    system("$cmd");	
	    
	}
	close $fh or warn "$0: close $df: $!";
	system("mv $df $path/Processed_dev");
    }
    system("rm -rf $dir/lock");
}

# Test
$dir = "$path/Input_test";
$server = 'usav1svsqlt70';
@files = open_dir($dir);
unless(-e "$dir/lock"){
    system("echo '' > $dir/lock"); 
    foreach my $file (@files){
	my $df = "$dir/$file";
	open my $fh, "<", $df or do {
	    warn "$0: open $df: $!";
	    next;
	};
	while(<$fh>){
	    chomp;
	    my @l = split(/\t/, $_);
	    $l[1] =~ s/\s/_/g;
	    my $cmd = "perl $path/remove_accession.pl $l[0] $l[1] $l[2] $server >> $path/logs_test/$log";
	    system("$cmd");	

	}
	close $fh or warn "$0: close $df: $!";	
	system("mv $df $path/Processed_test");
    }
    system("rm -rf $dir/lock");
}

# Stage
$dir = "$path/Input_stage";
$server = 'usav1svhaps70';
@files = open_dir($dir);
unless(-e "$dir/lock"){
    system("echo '' > $dir/lock"); 
    foreach my $file (@files){
	my $df = "$dir/$file";
	open my $fh, "<", $df or do {
	    warn "$0: open $df: $!";
	    next;
	};
	while(<$fh>){
	    chomp;
	    my @l = split(/\t/, $_);
	    $l[1] =~ s/\s/_/g;
	    my $cmd = "perl $path/remove_accession.pl $l[0] $l[1] $l[2] $server >> $path/logs_stage/$log";
	    system("$cmd");	

	}
	close $fh or warn "$0: close $df: $!";	
	system("mv $df $path/Processed_stage");
    }
    system("rm -rf $dir/lock");
}


# Production
$dir = "$path/Input";
$server = 'usav1svhapp70';
@files = open_dir($dir);
unless(-e "$dir/lock"){
    system("echo '' > $dir/lock"); 
    foreach my $file (@files){
	my $df = "$dir/$file";
	open my $fh, "<", $df or do {
	    warn "$0: open $df: $!";
	    next;
	};
	while(<$fh>){
	    chomp;
	    my @l = split(/\t/, $_);
	    $l[1] =~ s/\s/_/g;
            if($l[1] eq ""){$l[1]="empty_note";}
	    my $cmd = "perl $path/remove_accession.pl $l[0] $l[1] $l[2] $server >> $path/logs/$log";
	    system("$cmd");	

	}
	close $fh or warn "$0: close $df: $!";	
	system("mv $df $path/Processed");
    }
    system("rm -rf $dir/lock");
}





exit;



###########################################################################
# Subroutines
###########################################################################

# open_dir
#
#   - given dirname, set dirhandle

sub open_dir {

    my($dirname) = @_;
    my $fh;
    my @files;

    unless(opendir($fh, $dirname)) {
        print "Cannot open directory $dirname\n";
        exit;
    }

    @files = grep ( !/^\.\.?$/, readdir $fh);

    closedir $fh;

    return @files;
}
