#!/usr/bin/perl

use strict;
use warnings;

#my $date = `date --date="today" +"%Y%m"`;
#my $log = 'log' . $date;
# Dev
my $dir = "/mnt/swu/sspipeline/Input_dev";
my @files = open_dir($dir);
my %ran = ();
foreach my $file (@files){
    my $server = 'dev';
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	if(exists $ran{"$l[0]:$l[1]"}){
	    print "\n$file:skip the run for the same genelist under the same path\t**cmd\n";
	}
	else{
	    my $cmd = "nohup perl /mnt/clinical/bin/VCPipeline/run-sensitiveVC.pl -i $l[0] --gene $l[1] --token $l[2] --api $server >> /mnt/swu/sspipeline/log &";
#	    my $cmd = "nohup perl /mnt/swu/sspipeline/temp.pl $l[0] $l[1] >> /mnt/swu/sspipeline/log &";
	    print "\n$file: $cmd\t**cmd\n";
	    print `$cmd`, "**execute\n";
	    $ran{"$l[0]:$l[1]"} = $file;
	}
    }
    close $fh or warn "$0: close $df: $!";

    system("mv $df /mnt/swu/sspipeline/Processed_dev");
}

# Test
$dir = "/mnt/swu/sspipeline/Input_test";
@files = open_dir($dir);
%ran = ();
foreach my $file (@files){
    my $server = 'test';
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	if(exists $ran{"$l[0]:$l[1]"}){
	    print "\n$file:skip the run for the same genelist under the same path\t**cmd\n";
	}
	else{
	    my $cmd = "nohup perl /mnt/clinical/bin/VCPipeline/run-sensitiveVC.pl -i $l[0] --gene $l[1] --token $l[2] --api $server >> /mnt/swu/sspipeline/log &";
	    print "\n$file: $cmd\t**cmd\n";
	    print `$cmd`, "**execute\n";
	    $ran{"$l[0]:$l[1]"} = $file;
	}
    }
    close $fh or warn "$0: close $df: $!";

    system("mv $df /mnt/swu/sspipeline/Processed_test");
}

# Stage
$dir = "/mnt/swu/sspipeline/Input_stage";
@files = open_dir($dir);
%ran = ();
foreach my $file (@files){
    my $server = 'stage';
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	if(exists $ran{"$l[0]:$l[1]"}){
	    print "\n$file:skip the run for the same genelist under the same path\t**cmd\n";
	}
	else{
	    my $cmd = "nohup perl /mnt/clinical/bin/VCPipeline/run-sensitiveVC.pl -i $l[0] --gene $l[1] --token $l[2] --api $server >> /mnt/swu/sspipeline/log &";
	    print "\n$file: $cmd\t**cmd\n";
	    print `$cmd`, "**execute\n";
	    $ran{"$l[0]:$l[1]"} = $file;
	}
    }
    close $fh or warn "$0: close $df: $!";

    system("mv $df /mnt/swu/sspipeline/Processed_stage");
}

# Production
$dir = "/mnt/swu/sspipeline/Input";
@files = open_dir($dir);
%ran = ();
foreach my $file (@files){
    my $server = 'production';
    my $df = "$dir/$file";
    open my $fh, "<", $df or do {
	warn "$0: open $df: $!";
	next;
    };
    while(<$fh>){
	chomp;
	my @l = split("\t", $_);

	if(exists $ran{"$l[0]:$l[1]"}){
	    print "\n$file:skip the run for the same genelist under the same path\t**cmd\n";
	}
	else{
	    my $cmd = "nohup perl /mnt/clinical/bin/VCPipeline/run-sensitiveVC.pl -i $l[0] --gene $l[1] --token $l[2] --api $server >> /mnt/swu/sspipeline/log &";
	    print "\n$file: $cmd\t**cmd\n";
	    print `$cmd`, "**execute\n";
	    $ran{"$l[0]:$l[1]"} = $file;
	}
    }
    close $fh or warn "$0: close $df: $!";

    system("mv $df /mnt/swu/sspipeline/Processed");
}



exit;



###########################################################################
# Subroutines
###########################################################################

# open_dir
#
#   - given dirname, set dirhandle

sub open_dir {

    my($dirname) = @_;
    my $fh;
    my @files;

    unless(opendir($fh, $dirname)) {
        print "Cannot open directory $dirname\n";
        exit;
    }

    @files = grep ( !/^\.\.?$/, readdir $fh);

    closedir $fh;

    return @files;
}
